package com.appetiser.challenge.data

import com.appetiser.challenge.data.db.MovieDao
import com.appetiser.challenge.data.networking.ItunesSearchApi
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MovieRepositoryTest {

    @RelaxedMockK
    lateinit var movieDao: MovieDao

    private lateinit var server: MockWebServer
    private lateinit var SUT: MovieRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        server = MockWebServer()
        server.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(server.url("/test/").toString())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val itunesSearchApi = retrofit.create(ItunesSearchApi::class.java)

        SUT = MovieRepository(itunesSearchApi, movieDao)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    @Test
    fun `If success, should save movies to database`() {
        server.enqueue(
            MockResponse().setBody("""{"results":[{"trackId":1,"trackName":"Star","artworkUrl60":"random.jpg","trackPrice":1.29,"currency":"USD","primaryGenreName":"Pop"}]}""")
        )

        runBlocking { SUT.refreshData() }

        verify(exactly = 1) { movieDao.insertAll(allAny()) }
    }

}