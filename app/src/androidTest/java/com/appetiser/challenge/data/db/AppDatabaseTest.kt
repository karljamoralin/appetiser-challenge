package com.appetiser.challenge.data.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.appetiser.challenge.data.Movie
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest {

    private lateinit var movieDao: MovieDao
    private lateinit var appDatabase: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()

        movieDao = appDatabase.movieDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeAndReadFromDatabase() {
        val movie1 = Movie("1", "trackName", "artWorkUrl", "10.99", "Horror", "Long description")

        val movie2 = Movie("2", "trackName", "artWorkUrl", "10.99", "Horror", "Long description")

        val movies = ArrayList<Movie>()
        movies.add(movie1)
        movies.add(movie2)

        movieDao.insertAll(movies)

        val movieFromDb = movieDao.getMovie("1")
        Assert.assertEquals(movie1, movieFromDb)
    }


}