package com.appetiser.challenge.di

import com.appetiser.challenge.data.MovieRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun getMovieRepository(): MovieRepository
}