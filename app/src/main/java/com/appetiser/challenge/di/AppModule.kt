package com.appetiser.challenge.di

import android.content.Context
import androidx.room.Room
import com.appetiser.challenge.AppProperties
import com.appetiser.challenge.data.MovieRepository
import com.appetiser.challenge.data.db.AppDatabase
import com.appetiser.challenge.data.db.MovieDao
import com.appetiser.challenge.data.networking.ItunesSearchApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    fun getContext(): Context {
        return context
    }

    @Singleton
    @Provides
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(AppProperties.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun getItunesSearchApi(retrofit: Retrofit): ItunesSearchApi {
        return retrofit.create(ItunesSearchApi::class.java)
    }

    @Provides
    fun getMovieRepository(itunesSearchApi: ItunesSearchApi, movieDao: MovieDao): MovieRepository {
        return MovieRepository(itunesSearchApi, movieDao)
    }

    @Singleton
    @Provides
    fun getDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "movie-browser-db"
        ).build()
    }

    @Singleton
    @Provides
    fun getMovieDao(appDatabase: AppDatabase): MovieDao {
        return appDatabase.movieDao()
    }

}