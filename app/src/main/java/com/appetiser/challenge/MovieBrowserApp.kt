package com.appetiser.challenge

import android.app.Application
import com.appetiser.challenge.di.AppComponent
import com.appetiser.challenge.di.AppModule
import com.appetiser.challenge.di.DaggerAppComponent
import timber.log.Timber

class MovieBrowserApp : Application() {

    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()
        setUpDependencyInjection()
        setUpLogger()
    }

    private fun setUpDependencyInjection() {
        component = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .build()
    }

    private fun setUpLogger() {
        Timber.plant(Timber.DebugTree())
    }

}