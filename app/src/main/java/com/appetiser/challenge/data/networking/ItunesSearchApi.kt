package com.appetiser.challenge.data.networking

import retrofit2.Call
import retrofit2.http.GET

interface ItunesSearchApi {
    @GET("search?term=star&amp;country=au&amp;media=movie&amp;all")
    fun getMovies(): Call<MovieResponseSchema>
}