package com.appetiser.challenge.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Movie(
    @PrimaryKey val id: String,
    val trackName: String,
    val artWorkUrl: String,
    val price: String,
    val genre: String,
    val description: String
)