package com.appetiser.challenge.data

import androidx.lifecycle.LiveData
import com.appetiser.challenge.data.db.MovieDao
import com.appetiser.challenge.data.networking.ItunesSearchApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class MovieRepository(
    private val itunesSearchApi: ItunesSearchApi,
    private val movieDao: MovieDao
) {
    val movies: LiveData<List<Movie>> = movieDao.getMovies()

    suspend fun refreshData() {
        withContext(Dispatchers.IO) {
            val call = itunesSearchApi.getMovies()
            Timber.d("Sending HTTP request to ${call.request().url()}")

            val response = call.execute()
            Timber.d("Got HTTP response: ${response.body()}")

            val moviesFromSchema = response.body()

            if (moviesFromSchema != null) {
                val movies = moviesFromSchema.results
                    .filter {
                        // Arbitrarily removing results with null values
                        //   so that results shown to user will not have missing details
                        it.trackId != null && it.trackName != null && it.trackPrice != null && it.longDescription != null
                    }.map {
                        Movie(
                            // trackId, trackName, and description will not be null as we've removed
                            //   those in the above filter, hence we'll use the !! operator below
                            it.trackId!!,
                            it.trackName!!,
                            it.artworkUrl100,
                            "${it.trackPrice} ${it.currency}",
                            it.primaryGenreName,
                            it.longDescription!!
                        )
                    }
                movieDao.insertAll(movies)
            }
        }
    }

}
