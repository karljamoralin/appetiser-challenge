package com.appetiser.challenge.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.appetiser.challenge.data.Movie

@Database(entities = [Movie::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}