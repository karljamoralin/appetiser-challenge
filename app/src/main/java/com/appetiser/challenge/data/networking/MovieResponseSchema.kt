package com.appetiser.challenge.data.networking

data class MovieResponseSchema(
    val results: List<Result>
)

data class Result(
    val trackId: String?,
    val trackName: String?,
    val artworkUrl100: String,
    val trackPrice: String?,
    val currency: String,
    val primaryGenreName: String,
    val longDescription: String?
)