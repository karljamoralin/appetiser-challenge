package com.appetiser.challenge.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.challenge.data.Movie

@Dao
interface MovieDao {
    @Query("SELECT * from movie")
    fun getMovies(): LiveData<List<Movie>>

    @Query("SELECT * from movie WHERE id = :id")
    fun getMovie(id: String): Movie

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<Movie>)
}