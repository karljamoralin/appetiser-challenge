package com.appetiser.challenge.ui

import androidx.appcompat.app.AppCompatActivity
import com.appetiser.challenge.MovieBrowserApp
import com.appetiser.challenge.di.AppComponent


abstract class BaseActivity : AppCompatActivity() {
    protected val appComponent: AppComponent
        get() = (application as MovieBrowserApp).component
}