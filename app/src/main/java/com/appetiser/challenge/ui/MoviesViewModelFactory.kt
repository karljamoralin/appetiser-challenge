package com.appetiser.challenge.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appetiser.challenge.data.MovieRepository

class MoviesViewModelFactory(private val movieRepository: MovieRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val viewModel = modelClass.cast(MoviesViewModel(movieRepository))

        if (viewModel != null) {
            return viewModel
        } else {
            throw IllegalArgumentException("ViewModel not found")
        }
    }
}