package com.appetiser.challenge.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.appetiser.challenge.data.Movie
import com.appetiser.challenge.data.MovieRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class MoviesViewModel(private val movieRepository: MovieRepository) : ViewModel() {
    val isRefreshing: MutableLiveData<Boolean> = MutableLiveData()
    val movies: LiveData<List<Movie>> = movieRepository.movies

    fun refreshData(exceptionHandler: CoroutineExceptionHandler) {
        viewModelScope.launch(exceptionHandler) {
            isRefreshing.value = true
            movieRepository.refreshData()
            isRefreshing.value = false
        }
    }
}