package com.appetiser.challenge.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.challenge.R
import com.appetiser.challenge.data.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*


class MoviesAdapter(private val listener: MoviesAdapterListener) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    interface MoviesAdapterListener {
        fun onMovieClicked(id: String)
    }

    private var movies: List<Movie> = ArrayList()

    class ViewHolder(val movieView: View) : RecyclerView.ViewHolder(movieView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val movieView = LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_movie,
                parent,
                false
            )

        return ViewHolder(movieView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val view = holder.movieView
        val movie = movies[position]

        view.trackName.text = movie.trackName
        view.price.text = movie.price
        view.genre.text = movie.genre

        view.setOnClickListener {
            listener.onMovieClicked(movies[position].id)
        }

        Picasso.get()
            .load(movie.artWorkUrl)
            .placeholder(R.drawable.placeholder)
            .into(view.artwork)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun setMovies(data: List<Movie>) {
        this.movies = data
        notifyDataSetChanged()
    }
}
