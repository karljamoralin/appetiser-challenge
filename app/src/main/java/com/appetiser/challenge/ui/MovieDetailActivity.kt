package com.appetiser.challenge.ui

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.appetiser.challenge.R
import com.appetiser.challenge.data.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : BaseActivity() {

    companion object {
        const val ARG_ID = "arg_id"
    }

    private lateinit var id: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        id = intent.getStringExtra(ARG_ID)
            ?: throw IllegalArgumentException("Movie ID should be passed")

        setUpViewModel()
    }

    private fun setUpViewModel() {
        val model = ViewModelProvider(
            this,
            MoviesViewModelFactory(appComponent.getMovieRepository())
        )[MoviesViewModel::class.java]

        model.movies.observe(this, Observer<List<Movie>> { movies ->
            val movie = movies.find { it.id == id }

            if (movie != null) {
                trackName.text = movie.trackName
                price.text = movie.price
                genre.text = movie.genre
                description.text = movie.description

                Picasso.get()
                    .load(movie.artWorkUrl)
                    .into(artwork)
            }

            // TODO: For future improvement -- in the rare case that the movie is not found, handle it gracefully

        })
    }
}
