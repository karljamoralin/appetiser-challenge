package com.appetiser.challenge.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.core.content.edit
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.challenge.R
import com.appetiser.challenge.data.Movie
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_movie_browser.*
import kotlinx.coroutines.CoroutineExceptionHandler
import timber.log.Timber
import java.text.DateFormat

class MovieBrowserActivity : BaseActivity(), MoviesAdapter.MoviesAdapterListener {

    private lateinit var moviesAdapter: MoviesAdapter
    private lateinit var moviesViewModel: MoviesViewModel
    private lateinit var preferences: SharedPreferences

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Timber.e(throwable, "Error refreshing data")

        Snackbar.make(
            layout,
            "Error refreshing data",
            Snackbar.LENGTH_INDEFINITE
        ).setAction(getString(R.string.retry)) {
            refreshData()
        }.show()

        swipeRefreshLayout.isRefreshing = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_browser)
        setUpViewModel()
        setUpRecyclerView()
        preferences = getPreferences(Context.MODE_PRIVATE)
        swipeRefreshLayout.setOnRefreshListener { refreshData() }
        refreshData()
    }

    override fun onMovieClicked(id: String) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.ARG_ID, id)

        this.startActivity(intent)
    }

    private fun setUpViewModel() {
        moviesViewModel = ViewModelProvider(
            this,
            MoviesViewModelFactory(appComponent.getMovieRepository())
        )[MoviesViewModel::class.java]

        moviesViewModel.movies.observe(this, Observer<List<Movie>> { movies ->
            Timber.d("Got ${movies.size} movies")
            moviesAdapter.setMovies(movies)
        })

        moviesViewModel.isRefreshing.observe(
            this,
            Observer { swipeRefreshLayout.isRefreshing = it }
        )
    }

    override fun onResume() {
        super.onResume()
        setLastVisitText()
    }

    private fun refreshData() {
        moviesViewModel.refreshData(exceptionHandler)
    }

    private fun setUpRecyclerView() {
        moviesAdapter = MoviesAdapter(this)

        recyclerView.apply {
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = moviesAdapter
        }
    }

    private fun setLastVisitText() {
        val prefLastVisit = "pref_last_visit"

        if (preferences.contains(prefLastVisit)) {
            val time = preferences.getLong(prefLastVisit, 0L)
            val lastVisitString = "Last visit: ${DateFormat.getDateTimeInstance().format(time)}"

            lastVisit.visibility = View.VISIBLE
            lastVisit.text = lastVisitString
            preferences.edit { putLong(prefLastVisit, System.currentTimeMillis()) }
        } else {
            lastVisit.visibility = View.GONE
            preferences.edit { putLong(prefLastVisit, System.currentTimeMillis()) }
        }
    }

}
