This is the response to the Appetiser challenge, which is to create a master-detail app using the iTunes endpoint: https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all

Some features and decisions I'd like to note in this app:
- Dependency injection using dagger is used as this is useful to manage object dependencies in a scalable, clean manner
- The app is built with activities in MVVM pattern -- this makes easier data loading/retrieval that can survive configuration changes
- Room is used to persist API data as it's easy to setup and has LiveData support. SharedPreferences is used to persist "last visit" time as this is only a small amount of data.
- Retrofit is used to easily handle API calls
- Unit tests are limited to the repository class. This can be detailed further with negative tests, and by adding UI tests
- Picasso is used to handle image fetching as it can cache images and can put placeholders while images are loading


Conventions
- Camel case is used in naming XML layout elements. This is because the view type and which layout it belongs are already shown in autocomplete
